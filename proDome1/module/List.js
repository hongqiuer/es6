/**
 * Created by hqer on 2017/3/28.
 */
import React from 'react'
import {Link} from 'react-router-dom'
import {Table } from 'react-bootstrap'
import PageArea from '../componentUI/js/pageArea'

class List extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            pageNum:1,
            totalPage:1,
            title:[],
            data:[]
        };
        this._getInfo = this._getInfo.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
    }
    componentDidMount(){
        this._getInfo(this.state.pageNum);
    }
    _getInfo(_num){
        let parms = {pageNum:_num};
        window.GLOBAL.ajaxMap.setParms("list",parms);
        let ajax = window.GLOBAL.ajaxMap.getAjaxParms("list");
        ajax.success=function(r){
            this.setState({
                totalPage:r.totalPage,
                title:r.title,
                data:r.data,
                pageNum:_num
            });
        }.bind(this);
        $.ajax(ajax);
    }
    handleSelect(pageNow){
        this._getInfo(pageNow);
    }
    render() {
        return (
            <div className="apiList">
                <div className="midBox">
                    <Table striped hover responsive className="basicList">
                        <thead>
                        <tr>
                            {this.state.title.map((item,i) => (
                                <th className={"th"+i} key={i}>{item.text}</th>
                            ))}
                        </tr>
                        </thead>
                        <tbody>
                        {this.state.data.length?(
                            this.state.data.map((element,index) => (
                                <tr key={index}>
                                    {
                                        this.state.title.map((e,i) => (
                                            e.text==="详情"?(
                                                <td className={"th"+i} key={i}><Link to={"/Detail/"+element[e.key]}>详情</Link></td>
                                            ):(
                                                <td className={"th"+i} key={i}>{element[e.key]}</td>
                                            )
                                        ))
                                    }
                                </tr>
                            ))
                        ):(
                            <tr><td className="noData" colSpan={this.state.title.length}>暂无数据</td></tr>
                        )}
                        </tbody>
                    </Table>
                    <PageArea
                        totalPage={this.state.totalPage}
                        maxPage={5}
                        pageNow={this.state.pageNum}
                        onSelect={this.handleSelect}
                        onSearch={this.handleSelect}
                    />
                </div>
                {this.props.children}
            </div>
        )
    }
}
module.exports = List;
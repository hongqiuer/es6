/**
 * Created by hqer on 2017/4/28.
 */
import '../less/EmailInput.less'
import React from 'react'
import {findDOMNode} from 'react-dom'
class EmailInput extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            showStatus:false,
            v: this.props.v,
            _oldV:this.props.v
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleBlur = this.handleBlur.bind(this);
        this.handleKeyUp = this.handleKeyUp.bind(this);
        this.handleFocus = this.handleFocus.bind(this);
        this._clickAway = this._clickAway.bind(this);
    }
    componentWillReceiveProps(newProps){
        if(newProps.v!=this.state.v){
            this.setState({v:newProps.v});
        }
    }
    componentWillUnmount(){
        $(document).off("click");
    }
    handleChange(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value;
        if(_val!==""&&(!/^(([a-zA-Z0-9])([a-zA-Z0-9\_\.])*(\@|(\@[0-9a-zA-Z]+(\.[a-zA-Z]{0,3})*))?)$/.test(_val)||_val.indexOf("..")>-1)){
            this.setState({v:this.state._oldV})
        }else{
            this.setState({v:_val,_oldV:_val})
        }
    }
    handleBlur(event){
        if(typeof this.props.onBlur==="function"){
            this.props.onBlur(this);
        }
    }
    handleKeyUp(event){
        if(typeof this.props.onKeyUp==="function"){
            this.props.onKeyUp(this);
        }
    }
    handleFocus(event){
        this.setState({showStatus:true});
    }
    selectItem(_item){
        let _v = this.state.v.split("@")[0]+"@"+_item;
        this.setState({showStatus:false,v:_v});
    }
    _clickAway(){
        $(document).off("click").on("click",function(event){
            var _el = findDOMNode(this);
            if($(_el).hasClass("showUl")){
                var _eventTarget = event.srcElement ? event.srcElement:event.target;
                if (_eventTarget!==_el && $(_el).find(_eventTarget).length==0) {
                    this.setState({showStatus:false});
                }
            }
        }.bind(this));
    }
    render() {
        let other = Object.assign({},this.props);
        delete other.type;
        delete other.className;
        delete other.v;
        delete other.mailList;
        return (
            <div className={this.state.showStatus&&this.state.v?"EmailInput showUl":"EmailInput"} onClick={this._clickAway}>
                <input
                    {...other}
                    type="text"
                    className={this.props.className}
                    value={this.state.v}
                    onKeyUp={this.handleKeyUp}
                    onFocus={this.handleFocus}
                    onBlur={this.handleBlur}
                    onChange={this.handleChange}
                    />
                {this.state.showStatus&&this.state.v?(
                    <div className="EmailList">
                        <ul>
                            {this.props.mailList.map((item,i) => (
                                this.state.v.indexOf("@")===-1?(
                                    <li key={i} onClick={this.selectItem.bind(this,item)}>{this.state.v.split("@")[0]+"@"+item}</li>
                                ):(
                                    item.indexOf(this.state.v.split("@")[1])===0?(
                                        <li key={i} onClick={this.selectItem.bind(this,item)}>{this.state.v.split("@")[0]+"@"+item}</li>
                                    ):""
                                )
                            ))}
                        </ul>
                    </div>
                ):""}
            </div>

        )
    }
}

EmailInput.propTypes = {
    type: React.PropTypes.string.isRequired
};
EmailInput.defaultProps = {
    className:"",
    v:"",
    mailList:["139.com","qq.com","163.com","sina.com","sina.cn","hotmail.com","gmail.com","sohu.com","126.com"]
};
module.exports = EmailInput;
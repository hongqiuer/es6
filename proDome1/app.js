/**
 * Created by hqer on 2017/3/28.
 */
import './less/main.less'
import React from 'react'
import { render } from 'react-dom'
import {HashRouter as Router,Route} from 'react-router-dom'

import MockData from '../Mock/proDome1/mockData'

import Global from './config/global'
import List from "./module/List"
import Detail from "./module/Detail"

render((
    <Router>
        <div>
            <Route path="/" component={List}/>
            <Route path="/Detail/:id" component={Detail}/>
        </div>
    </Router>
), document.getElementById('app'));

/**
 * Created by hqer on 2017/4/27.
 */
import React from 'react'
import UIDropDown from "../componentUI/js/DropDown"
import UIDropDownAddSearch from "../componentUI/js/DropDownAddSearch"
class DropDown extends React.Component{
    render() {
        return (
            <div className="DropDown">
                <h2>下拉列表UI组件</h2>
                <h3>样例：</h3>
                <div className="inputLine">普通下拉列表：<UIDropDown className="InputUI" v="" a={[{t:"选项一",v:"abc"},{t:"选项二",v:"bcd"},{t:"选项三",v:"cde"}]}/></div>
                <div className="inputLine">带检索功能下拉列表：<UIDropDownAddSearch className="InputUI" v="" a={[{t:"选项一",v:"abc"},{t:"选项二",v:"bcd"},{t:"选项三",v:"cde"},{t:"1选项一",v:"abc1"},{t:"1选项二",v:"bcd1"},{t:"1选项三",v:"cde1"},{t:"2选项一",v:"2abc"},{t:"2选项二",v:"2bcd"},{t:"2选项三",v:"2cde"},{t:"3选项一",v:"3abc"},{t:"3选项二",v:"3bcd"},{t:"3选项三",v:"3cde"},{t:"选项一一",v:"abc1"},{t:"选项一二",v:"bcd2"},{t:"选项二三",v:"cde3"}]}/></div>
            </div>
        )
    }
}
module.exports = DropDown;
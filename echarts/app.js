import React from 'react'
import { render } from 'react-dom'
import {HashRouter as Router,Route} from 'react-router-dom'

import TopComponent from "./module/TopComponent"
import Zj from "./module/zjComponent"

render((
    <Router>
        <TopComponent>
            <Route path="/" component={Zj}/>
        </TopComponent>
    </Router>
), document.getElementById('app'));

/**
 * Created by hqer on 2017/3/14.
 */
import React from 'react'

class TopComponent extends React.Component{
    componentDidMount(){
        if($(".adaptive")[0]){
            let _d = $(".adaptive").width()/1920;
            $(".contentBox").css({transform:"scale("+_d+")"});
            $(window).resize(function(){
                let _d = $(".adaptive").width()/1920;
                $(".contentBox").css({transform:"scale("+_d+")"});
            })
        }
    }
    render() {
        return (
            <div className="contentBox">
                {this.props.children}
            </div>
        )
    }
}
module.exports = TopComponent;
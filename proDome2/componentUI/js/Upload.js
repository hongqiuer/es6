/**
 * Created by hqer on 2017/5/17.
 */
import '../less/Upload.less'
import React from 'react'
import {Button } from 'react-bootstrap'
import FileUpload from 'react-fileupload'
class Upload extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            name:this.props.name,
            text: this.props.chooseAndUpload?"选择文件":"上传文件",
            errorTips:"",
            status:this.props.status
        };
    }
    render() {
        let other = Object.assign({},this.props);
        delete other.v;
        delete other.s;
        delete other.style;
        delete other.options;
        let options = {
            baseUrl:this.props.baseUrl,
            param:this.props.param,
            accept:this.props.accept,
            chooseAndUpload:this.props.chooseAndUpload,
            fileFieldName:"upload",
            beforeChoose:function(){
                console.log('=====START======');
                if(typeof this.props.beforeChoose==="function"){
                    this.props.beforeChoose(this);
                }
            }.bind(this),
            chooseFile:function(files){
                console.log('=====chooseFile======');
                if(typeof this.props.chooseFile==="function"){
                    this.props.chooseFile(this,files);
                }else if(typeof files == "String"){
                    this.setState({name:files,errorTips:""})
                }else{
                    this.setState({name:files[0].name,errorTips:""})
                }
            }.bind(this),
            beforeUpload:function(files , mill){
                console.log('=====beforeUpload======');
                if(typeof this.props.beforeUpload==="function"){
                    this.props.beforeUpload(this,files,mill);
                }else if(!files||files.length===0){
                    this.setState({errorTips:"* 请选择上传的文件!",status:false,name:""});
                    return false;
                }else if(files[0].size === 0){
                    this.setState({errorTips:"* 文件大小为0,请重新上传!",status:false,name:""});
                    return false;
                }else if(files[0].size > this.props.maxSize){
                    this.setState({errorTips:"* 文件过大,请重新上传!",status:false,name:""});
                    return false;
                }
                return true;
            }.bind(this),
            doUpload : function(files,mill,xhrID){
                console.log('=====doUpload======');
                if(typeof this.props.doUpload==="function"){
                    this.props.doUpload(this,files,mill,xhrID);
                }else{
                    this.setState({text:"上传中...",errorTips:"",status:false});
                }
            }.bind(this),
            uploadSuccess : function(resp){
                console.log('=====uploadSuccess======');
                if(typeof this.props.uploadSuccess==="function"){
                    this.props.uploadSuccess(this,resp);
                }else if(resp.code){
                    this.setState({text:this.props.chooseAndUpload?"选择文件":"上传文件",errorTips:"* 上传成功!",status:true,name:resp.data});
                }else{
                    this.setState({text:this.props.chooseAndUpload?"选择文件":"上传文件",errorTips:"* 后台出错,请重新上传!",status:false,name:""});
                }
            }.bind(this),
            uploadError : function(err){
                console.log("uploadError===="+err.message);
                if(typeof this.props.uploadError==="function"){
                    this.props.uploadError(this,err);
                }else{
                    this.setState({text:this.props.chooseAndUpload?"选择文件":"上传文件",errorTips:"* 上传失败,请重新上传!",status:false,name:""});
                }
            }.bind(this),
            uploadFail : function(resp){
                console.log("uploadFail===="+resp);
                if(typeof this.props.uploadFail==="function"){
                    this.props.uploadFail(this,resp);
                }else{
                    this.setState({text:this.props.chooseAndUpload?"选择文件":"上传文件",errorTips:"* 上传失败,请重新上传!",status:false,name:""});
                }
            }.bind(this)
        };
        return (
            <div style={this.props.style} className="UploadUI">
                <input className="UploadStatus" type="hidden" value={this.state.status}/>
                <input type="text" readOnly className={this.props.className} value={this.state.name}/>
                    {
                        this.props.chooseAndUpload?(
                            <FileUpload options={options} className="UploadBtn">
                                <Button ref="chooseAndUpload">{this.state.text}</Button>
                            </FileUpload>
                        ):(
                        <FileUpload options={options} className="UploadBtn">
                            <Button ref="chooseBtn">选择文件</Button>
                            <Button ref="uploadBtn">{this.state.text}</Button>
                        </FileUpload>
                        )
                    }
                <span className="errorTips">{this.state.errorTips}</span>
            </div>
        )
    }
}

Upload.propTypes = {
    baseUrl:React.PropTypes.string.isRequired
};
Upload.defaultProps = {
    chooseAndUpload:true,
    baseUrl:"http://127.0.0.1:8415/mock/devPortal/getUploadFile",
    maxSize:2*1048576,
    accept:"",
    param:{},
    name:"",
    status:false,
    className:""
};
module.exports = Upload;
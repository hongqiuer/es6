/**
 * Created by hqer on 2017/4/27.
 */
import React from "react"
import UIInput from "../componentUI/js/Input"
import EmailInput from "../componentUI/js/EmailInput"
class Input extends React.Component{
    render() {
        return (
            <div className="Input">
                <h2>输入框UI组件</h2>
                <h3>样例：</h3>
                <div className="inputLine">数字输入框(能输入“-”和0~9的数字)：<UIInput type="number" className="InputUI"/></div>
                <div className="inputLine">金额输入框(保留两位小数)：<UIInput type="money" className="InputUI" maxValue="999999999"/></div>
                <div className="inputLine">正整数输入框：<UIInput type="positiveInt" className="InputUI" maxValue="999999999"/></div>
                <div className="inputLine">自然数输入框：<UIInput type="naturalNum" className="InputUI" maxValue="999999999"/></div>
                <div className="inputLine">邮箱输入框：<EmailInput className="InputUI"/></div>
            </div>
        )
    }
}
module.exports = Input;
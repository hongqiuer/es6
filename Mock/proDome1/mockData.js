/**
 * Created by hqer on 2017/3/28.
 */
import Mock from 'mockjs'
import AjaxMap from '../../proDome1/config/ajaxMap'
Mock.mock(
    AjaxMap.getAjaxUrl("list"),
    {
        "totalPage|1-100": 100,
        "title": [
            {"text": "组件ID", "key": "id"},
            {"text": "省份", "key": "region"},
            {"text": "类型", "key": "type"},
            {"text": "ip:port", "key": "ip"},
            {"text": "状态", "key": "status"},
            {"text": "详情", "key": "url"}
        ],
        "data|10": [
            {
                "id":"@string(number,12)",
                "region": "@county(true)",
                "type": "类型@integer(1,10)",
                "ip": "@ip()",
                "status":"状态@integer(1,10)",
                "url": "@string(number,12)"
            }
        ]
    }
);
Mock.mock(
    AjaxMap.getAjaxUrl("detail"),
    {
        "totalPage|1-20": 20,
        "title": [
            {"text": "api", "key": "api"},
            {"text": "南向path", "key": "path"}
        ],
        "data|10": [
            {
                "api": "@string(number,12)",
                "path": "@url()"
            }
        ]
    }
);
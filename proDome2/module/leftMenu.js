/**
 * Created by hqer on 2017/4/27.
 */
import React from 'react'
import { NavLink  } from 'react-router-dom'

class Left extends React.Component{
    render() {
        return (
            <div className="LeftMenu">
                <ul>
                    <NavLink  to="/OutLine" activeClassName="selected" exact><li>概要</li></NavLink>
                    <NavLink  to="/Input" activeClassName="selected" exact><li>输入框</li></NavLink>
                    <NavLink  to="/DropDown" activeClassName="selected" exact><li>下拉列表</li></NavLink>
                    <NavLink  to="/CheckBox" activeClassName="selected" exact><li>复选框</li></NavLink>
                    <NavLink  to="/Upload" activeClassName="selected" exact><li>下载组件</li></NavLink>
                    <NavLink  to="/Pagination" activeClassName="selected" exact><li>分页组件</li></NavLink>
                </ul>
            </div>
        )
    }
}
module.exports = Left;
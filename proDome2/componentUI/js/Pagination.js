/**
 * Created by hqer on 2017/5/19.
 */
import '../less/Pagination.less'
import React from 'react'
import { findDOMNode } from 'react-dom'
import { Link } from 'react-router-dom'
import { Pagination,Button } from 'react-bootstrap'
import InputUI from '../../componentUI/js/Input'
import DropDownUI from '../../componentUI/js/DropDown'

class PaginationArea extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            pageNow:this.props.pageNow,
            pageSize:this.props.pageSize
        };
        this.handleSelect = this.handleSelect.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
        this._setInputVal = this._setInputVal.bind(this);
        this._returnSize = this._returnSize.bind(this);
    }
    componentWillReceiveProps(newProps){
        if(newProps.pageNow!=this.state.pageNow&&newProps.i!=this.state.i){
            this.setState({pageNow:newProps.pageNow,i:newProps.i});
        }else if(newProps.pageNow!=this.state.pageNow){
            this.setState({pageNow:newProps.pageNow});
        }else if(newProps.i!=this.state.i){
            this.setState({i:newProps.i});
        }
    }
    _setInputVal(_obj){
        if(!_obj.state.v){
            _obj.setState({v:this.state.pageNow});
        }
    }
    handleSelect(i){
        if(this.state.pageNow!=i){
            if(typeof this.props.onSelect === "function"){
                this.props.onSelect(i,this.state.pageSize);
            }else{
                this.setState({pageNow:i});
            }
        }
    }
    handleSearch(){
        let _e = findDOMNode(this);
        let _pageN = $(_e).find("input.pageInput").val();
        if(_pageN*1&&this.state.pageNow!=_pageN*1){
            if(typeof this.props.onSearch === "function"){
                this.props.onSearch(_pageN*1,this.state.pageSize);
            }else{
                this.setState({pageNow:_pageN});
            }
        }
    }
    _returnSize(_v){
        this.setState({pageSize:_v});
    }
    render() {
        return (
            <div className="PaginationUI">
                {
                    this.props.pageSizeList.length>0?(
                        <div className="pageSizeArea">
                            <span className="pageText">每页 </span>
                            <DropDownUI className="dropDown" v={this.state.pageSize} a={this.props.pageSizeList} returnValue={this._returnSize}/>
                            <span className="pageText">条</span>
                        </div>
                    ):""
                }
                <Pagination
                    prev
                    next
                    items={this.props.totalPage}
                    maxButtons={this.props.maxPage}
                    activePage={this.state.pageNow}
                    onSelect={this.handleSelect}
                    className="basicPagination"/>
                <span className="pageText">共 {this.props.totalPage}页 到第</span>
                <InputUI type="positiveInt" maxValue={this.props.totalPage} className="dropDown" v={this.state.pageNow} onBlur={this._setInputVal}/>
                <span className="pageText">页</span>
                <Button onClick={this.handleSearch}>确定</Button>
            </div>
        )
    }
}

PaginationArea.propTypes = {
    totalPage: React.PropTypes.number.isRequired,
    maxPage: React.PropTypes.number.isRequired,
    pageNow: React.PropTypes.number.isRequired,
    pageSize: React.PropTypes.number.isRequired,
    pageList: React.PropTypes.array.isRequired
};
PaginationArea.defaultProps = {
    totalPage:1,
    maxPage:1,
    pageNow:1,
    pageSize:10,
    pageSizeList:[{t:10,v:10},{t:30,v:30},{t:50,v:50}]
};
module.exports = PaginationArea;
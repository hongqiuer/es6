var webpack = require('webpack');

module.exports = {
   entry: {
      vendors: ['react','react-dom','react-router','react-bootstrap','babel-polyfill'],
      index:[__dirname+'/app.js']
   },
   output: {
      path: __dirname+'/dist/',
      filename: '[name].js',
      chunkFilename: '[id].chunk.js',
      publicPath: 'dist/'
   },
   devServer: {
      contentBase:__dirname,
      inline: true,
      port: 8888,
      host:'0.0.0.0'
   },
   module: {
      loaders: [
         {
            test: /\.js?$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            query: {
               plugins:[
                  "transform-runtime",
                  ["transform-es2015-classes",{"loose":true}],
                  "transform-proto-to-assign",
                  "transform-es3-property-literals",
                  "transform-es3-member-expression-literals",
                  "transform-es2015-modules-simple-commonjs"
               ],
               presets: ['es2015', 'react']
            }
         },
         {
            test: /\.json$/,
            loader: 'json-loader'
         },
         {
            test: /\.css$/,loader: 'style-loader!css-loader!autoprefixer-loader'
         },
         {
            test: /\.less/,loaders: 'style-loader!css-loader!autoprefixer-loader!less-loader'
         }
      ]
   },
   plugins: [
      new webpack.DefinePlugin({
         'process.env':{
            'NODE_ENV': JSON.stringify('production')
         }
      }),
      new webpack.optimize.CommonsChunkPlugin({
         name:'vendors',
         chunks:['vendors']
      })/*,
       new webpack.optimize.UglifyJsPlugin({
       compress: {
       warnings: false
       }
       })*/
   ]
};

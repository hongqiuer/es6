/**
 * Created by hqer on 2017/5/19.
 */
import React from 'react'
import PaginationUI from "../componentUI/js/Pagination"
class Pagination extends React.Component{
    render() {
        return (
            <div className="Upload">
                <h2>分页组件</h2>
                <h3>样例：</h3>
                <div className="inputLine">分页有选择显示条数：<PaginationUI totalPage={10} maxPage={3} pageNow={1} /></div>
                <div className="inputLine">分页无选择显示条数：<PaginationUI totalPage={10} maxPage={3} pageNow={1} pageSizeList={[]}/></div>
            </div>
        )
    }

}
module.exports = Pagination;
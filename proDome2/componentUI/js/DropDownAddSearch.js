/**
 * Created by hqer on 2017/5/16.
 */
import '../less/DropDown.less'
import React from 'react'
import {findDOMNode } from 'react-dom'
import {Glyphicon } from 'react-bootstrap'


class DropDownAddSearch extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            v: this.props.v,
            sv:this.props.v,
            isShowList: false,
            isSearch:false
        };
        this.selectItem = this.selectItem.bind(this);
        this._clickAway = this._clickAway.bind(this);
        this.toggle = this.toggle.bind(this);
        this.changeSearchState = this.changeSearchState.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }
    componentWillReceiveProps(newProps){
        if(newProps.v!=this.state.v){
            this.setState({v:newProps.v,sv:newProps.v,isShowList:false,isSearch:false});
        }
    }
    componentWillUnmount(){
        $(document).off("click");
    }
    selectItem(_v){
        if(typeof this.props.returnValue==="function"){
            this.props.returnValue(_v);
        }else{
            this.setState({isShowList:false,v:_v,isSearch:false});
        }
    }
    _clickAway(){
        $(document).off("click").on("click",function(event){
            var _el = findDOMNode(this);
            if($(_el).hasClass("showUl")){
                var _eventTarget = event.srcElement ? event.srcElement:event.target;
                if (_eventTarget!==_el && $(_el).find(_eventTarget).length==0) {
                    this.setState({isShowList:false,isSearch:false});
                }
            }
        }.bind(this));
    }
    toggle(event){
        var _el = findDOMNode(this);
        $(_el).find("input").focus();
        if(this.state.isShowList&&!this.state.isSearch){
            this.setState({isShowList:false});
        }else{
            this.setState({isShowList:true});
        }
    }
    changeSearchState(event){
        var _el = findDOMNode(this);
        $(_el).find("input").focus();
        if(this.state.isSearch){
            this.setState({isSearch:false});
        }else{
            this.setState({isShowList:true,isSearch:true,sv:this.props.a.map((n,i)=>(n.v === this.state.v?n.t:"")).toString().replace(/\,/g,"")});
        }
    }
    handleChange(event){
        let _eT = event.srcElement?event.srcElement:event.target;
        let _val = _eT.value;
        this.setState({sv:_val});
    }
    render() {
        let other = {readOnly:true};
        if(this.state.isSearch){
            delete other.readOnly;
        }
        return (
            <div className={this.state.isShowList?"DropDownUI showUl":"DropDownUI"} onClick={this._clickAway}>
                <input
                    {...other}
                    type="text"
                    className={this.props.className}
                    placeholder="请选择"
                    value={this.state.isSearch?this.state.sv:this.props.a.map((n,i)=>(n.v === this.state.v?n.t:"")).toString().replace(/\,/g,"")}
                    onClick={this.toggle}
                    onChange={this.handleChange}
                    />
                <div className={"icon"+(this.state.isShowList?" top":"")+(this.state.isSearch?" show":"")}>
                    {
                        this.state.isSearch?"":(
                            <Glyphicon glyph="chevron-down" onClick={this.toggle}/>
                        )
                    }
                    <Glyphicon glyph="search" onClick={this.changeSearchState}/>
                </div>
                {
                    this.state.isShowList?(
                        <div className="DropDownListArea">
                            <ul className="DropDownList">
                                {this.props.a.map((n,i) => (
                                    !this.state.isSearch||(this.state.isSearch&&this.state.sv==="")?(
                                        <li key={i} onClick={this.selectItem.bind(this,n.v)}>{n.t}</li>
                                    ):(
                                        n.t.indexOf(this.state.sv)>-1?(
                                            <li key={i} onClick={this.selectItem.bind(this,n.v)}>{n.t}</li>
                                        ):""
                                    )
                                ))}
                            </ul>
                        </div>
                    ):""
                }

            </div>
        )
    }
}

DropDownAddSearch.propTypes = {
    a: React.PropTypes.array.isRequired
};
DropDownAddSearch.defaultProps = {
    className:"",
    v:"",
    a:[]
};
module.exports = DropDownAddSearch;
/**
 * Created by hqer on 2017/4/27.
 */
import React from 'react'
import FileUploadUI from "../componentUI/js/Upload"
class Upload extends React.Component{
    render() {
        return (
            <div className="Upload">
                <h2>上传按钮UI组件</h2>
                <h3>样例：</h3>
                <div className="inputLine">自动上传：<FileUploadUI baseUrl="http://127.0.0.1:8415/mock/devPortal/getUploadFile" className="InputUI"/></div>
                <div className="inputLine">手动上传：<FileUploadUI baseUrl="http://127.0.0.1:8415/mock/devPortal/getUploadFile" className="InputUI" chooseAndUpload={false}/></div>
            </div>
        )
    }

}
module.exports = Upload;
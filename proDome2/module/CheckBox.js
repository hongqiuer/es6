/**
 * Created by hqer on 2017/4/27.
 */
import React from 'react'
import UICheckBox from "../componentUI/js/CheckBox"
import UIRadioBtn from "../componentUI/js/RadioBtn"
class CheckBox extends React.Component{
    render() {
        return (
            <div className="CheckBox">
                <h2>选择按钮UI组件</h2>
                <h3>样例：</h3>
                <div className="inputLine">复选框：<UICheckBox s={true} v="abc"/></div>
                <div className="inputLine">单选框：<UIRadioBtn s={false} v="abc"/></div>
            </div>
        )
    }
}
module.exports = CheckBox;
/**
 * Created by hqer on 2017/3/17.
 */
import React from 'react'
import { Row,Col } from 'react-bootstrap'

let timeTicket =null;
let timeClick0 = null;
let timeClick1 = null;
let timeClick2 = null;

let rootPath = "http://112.54.207.57:8888/open_demo";
if(document.cookie){
    var aCookies = document.cookie.split("; ");
    for(var i=0;i<aCookies.length;i++) {
        var aCrumb = aCookies[i].split("=");
        if(aCrumb[0]==="rootPath"){
            rootPath = decodeURIComponent(unescape(aCrumb[1]));
        }
    }
    aCookies = "";
}

class zjComponent extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            level:0,
            animation:[0,0,0],
            apiTotal:0,
            apiNum:0,
            apiRate: 0,
            delay: 0,
            userOfTaobao: 0,
            userOfNews: 0,
            userOfChinamobile: 0,
            manyVoice: 0,
            smsTemplate: 0,
            message: 0,
            apiTotalMaxRandDom:20,
            userOfTaobaoMaxRandDom:14,
            userOfNewsMaxRandDom:5,
            userOfChinamobileMaxRandDom:12,
            manyVoiceMaxRandDom:20,
            smsTemplateMaxRandDom:8,
            messageMaxRandDom:13,
            apiTotalFormat:[],
            userOfTaobaoFormat:[],
            userOfNewsFormat:[],
            userOfChinamobileFormat:[],
            manyVoiceFormat:[],
            smsTemplateFormat:[],
            messageFormat:[]
        };
        this.setNewData = this.setNewData.bind(this);
        this.fetchNewDate = this.fetchNewDate.bind(this);
        this.formatFun = this.formatFun.bind(this);
        this.getRanDom = this.getRanDom.bind(this);
        this.reAnimation0 = this.reAnimation0.bind(this);
        this.reAnimation1 = this.reAnimation1.bind(this);
        this.reAnimation2 = this.reAnimation2.bind(this);
    }
    componentDidMount(){
        if (timeTicket) {
            clearInterval(timeTicket);
        }
        this.setNewData();
        timeTicket = setInterval(this.fetchNewDate,3000);
    }
    componentDidUpdate(prevProps, prevState){
        if(prevState.manyVoiceFormat[prevState.manyVoiceFormat.length-3]!=this.state.manyVoiceFormat[this.state.manyVoiceFormat.length-3]){
            let _animation = this.state.animation;
            _animation[0] = 1;
            this.setState({animation:_animation});
            if(timeClick0){
                clearTimeout(timeClick0)
            }
            timeClick0 = setTimeout(this.reAnimation0,1500);
        }
        if(prevState.messageFormat[prevState.messageFormat.length-3]!=this.state.messageFormat[this.state.messageFormat.length-3]){
            let _animation = this.state.animation;
            _animation[1] = 1;
            this.setState({animation:_animation});
            if(timeClick2){
                clearTimeout(timeClick2)
            }
            timeClick2 = setTimeout(this.reAnimation1,1500);
        }
        if(prevState.smsTemplateFormat[prevState.smsTemplateFormat.length-3]!=this.state.smsTemplateFormat[this.state.smsTemplateFormat.length-3]){
            let _animation = this.state.animation;
            _animation[2] = 1;
            this.setState({animation:_animation});
            if(timeClick1){
                clearTimeout(timeClick1)
            }
            timeClick1 = setTimeout(this.reAnimation2,1500);
        }
    }

    componentWillUnmount() {
        if (timeTicket) {
            clearInterval(timeTicket);
        }
        if(timeClick0){
            clearTimeout(timeClick0);
        }
        if(timeClick1){
            clearTimeout(timeClick1);
        }
        if(timeClick2){
            clearTimeout(timeClick2);
        }
    }
    reAnimation0(){
        let _animation = this.state.animation;
        _animation[0] = 0;
        this.setState({animation:_animation});
    }
    reAnimation1(){
        let _animation = this.state.animation;
        _animation[1] = 0;
        this.setState({animation:_animation});
    }
    reAnimation2(){
        let _animation = this.state.animation;
        _animation[2] = 0;
        this.setState({animation:_animation});
    }
    formatFun(_num){
        let re=/\d{1,3}(?=(\d{3})+$)/g;
        let n1=_num.toString().replace(/^(\d+)((\.\d+)?)$/,function(s,s1,s2){return s1.replace(re,"$&,")+s2;});
        return n1.split("");
    }
    getRanDom(_dif){
        if(_dif > 20){
            return Math.floor(_dif/20)
        }
        if(_dif <= 20 && _dif >= 1){
            return 1;
        }
        if(_dif <= 0){
            return 0
        }
    }
    setRanDom(_ran){
        if(_ran <= 0){
            return 0;
        }else if(_ran === 1){
            return Math.round(Math.random());
        }else if(_ran === 2){
            return Math.round(Math.random()*1)+1;
        }else if(_ran > 2 && _ran < 6){
            return Math.round(Math.random()*2)+(_ran-2);
        }else if(_ran > 5){
            return Math.round(Math.random()*5)+(_ran-5);
        }
    }
    setNewData(){
        //this.setState({level:1});
        $.ajax({
            type: "get",
            url: rootPath+"/GetStaticticsServlet?callback=jsonpFun",
            dataType: "jsonp",
            jsonp: "callback",
            jsonpCallback:"jsonpFun",
            success:function(data){
                this.setState({
                    apiTotal:data.apiTotalLast,
                    apiNum:data.apiNum,
                    apiRate: data.apiRate,
                    delay: data.delay,
                    userOfTaobao: data.userOfTaobaoLast,
                    userOfNews: data.userOfNewsLast,
                    userOfChinamobile: data.userOfChinamobileLast,
                    manyVoice: data.manyVoiceLast,
                    smsTemplate: data.smsTemplateLast,
                    message: data.messageLast,
                    level:1,
                    apiTotalMaxRandDom:this.getRanDom(data.apiTotal-data.apiTotalLast),
                    userOfTaobaoMaxRandDom:this.getRanDom(data.userOfTaobao-data.userOfTaobaoLast),
                    userOfNewsMaxRandDom:this.getRanDom(data.userOfNews-data.userOfNewsLast),
                    userOfChinamobileMaxRandDom:this.getRanDom(data.userOfChinamobile-data.userOfChinamobileLast),
                    manyVoiceMaxRandDom:this.getRanDom(data.manyVoice-data.manyVoiceLast),
                    smsTemplateMaxRandDom:this.getRanDom(data.smsTemplate-data.smsTemplateLast),
                    messageMaxRandDom:this.getRanDom(data.message-data.messageLast),
                    apiTotalFormat:this.formatFun(data.apiTotalLast),
                    userOfTaobaoFormat:this.formatFun(data.userOfTaobaoLast),
                    userOfNewsFormat:this.formatFun(data.userOfNewsLast),
                    userOfChinamobileFormat:this.formatFun(data.userOfChinamobileLast),
                    manyVoiceFormat:this.formatFun(data.manyVoiceLast),
                    smsTemplateFormat:this.formatFun(data.smsTemplateLast),
                    messageFormat:this.formatFun(data.messageLast)
                })
            }.bind(this)
        });
    }
    fetchNewDate(){
        if(this.state.level===0){
            this.setNewData();
        }else{
            let level = this.state.level + 1;
            if(level === 20){
                level = 0;
            }
            let oldData = this.state;
            //console.log(this.state.level+"....."+JSON.stringify(oldData));
            oldData.apiTotal = oldData.apiTotal+this.setRanDom(oldData.apiTotalMaxRandDom);
            oldData.userOfTaobao=oldData.userOfTaobao+this.setRanDom(oldData.userOfTaobaoMaxRandDom);
            oldData.userOfNews=oldData.userOfNews+this.setRanDom(oldData.userOfNewsMaxRandDom);
            oldData.userOfChinamobile= oldData.userOfChinamobile+this.setRanDom(oldData.userOfChinamobileMaxRandDom);
            oldData.manyVoice= oldData.manyVoice+this.setRanDom(oldData.manyVoiceMaxRandDom);
            oldData.smsTemplate=oldData.smsTemplate+this.setRanDom(oldData.smsTemplateMaxRandDom);
            oldData.message=oldData.message+this.setRanDom(oldData.messageMaxRandDom);
            this.setState({
                level:level,
                apiTotal:oldData.apiTotal,
                userOfTaobao:oldData.userOfTaobao,
                userOfNews: oldData.userOfNews,
                userOfChinamobile: oldData.userOfChinamobile,
                manyVoice: oldData.manyVoice,
                smsTemplate: oldData.smsTemplate,
                message: oldData.message,
                apiTotalFormat:this.formatFun(oldData.apiTotal),
                userOfTaobaoFormat:this.formatFun(oldData.userOfTaobao),
                userOfNewsFormat:this.formatFun(oldData.userOfNews),
                userOfChinamobileFormat:this.formatFun(oldData.userOfChinamobile),
                manyVoiceFormat:this.formatFun(oldData.manyVoice),
                smsTemplateFormat:this.formatFun(oldData.smsTemplate),
                messageFormat:this.formatFun(oldData.message)
            });
        }
    }
    render() {
        //console.log(JSON.stringify(this.state));
        return (
            <div className="zj">
                <div className="topArea">
                    <h1>中国移动通信开放平台 打开运营商网络围墙</h1>
                    <h2>API累计调用次数</h2>
                    <div className="numLine big fl">
                        {this.state.apiTotalFormat.map((item,i) => (
                            item === ","?(
                                <div className="point" key={i}>{item}</div>
                            ):(
                                <div className={"numBox Index"+item}>
                                    <ul className="numList">
                                        <li key={0}>0</li>
                                        <li key={1}>1</li>
                                        <li key={2}>2</li>
                                        <li key={3}>3</li>
                                        <li key={4}>4</li>
                                        <li key={5}>5</li>
                                        <li key={6}>6</li>
                                        <li key={7}>7</li>
                                        <li key={8}>8</li>
                                        <li key={9}>9</li>
                                    </ul>
                                </div>
                            )
                        ))}
                    </div>
                    <div className="apiLine fl">
                        <div className="item">
                            活跃API总数量：<font color="#ffe082">{this.state.apiNum}</font>
                        </div>
                        <div className="item">
                            API调用成功率：<span>{this.state.apiRate}</span><font color="#ffe082">%</font>
                        </div>
                        <div className="item">
                            API调用时延：<span>{this.state.delay}</span><font color="#ffe082">s</font>
                        </div>
                    </div>
                </div>
                <div className="mapArea">
                    <Row>
                        <Col xs={6}>
                            <h2 className="title1">服务用户热度图</h2>
                            <img src="img/map.png"/>
                        </Col>
                        <Col xs={6}>
                            <h2 className="title2">能力调用排行榜</h2>
                            <ul className="imgArea">
                                <li className={this.state.animation[0]===1?"no1Box no1A":"no1Box"}>
                                    <div className="txt">
                                        NO.1<br/>拨打验证
                                    </div>
                                </li>
                                <li className={this.state.animation[1]===1?"no2Box no2A":"no2Box"}>
                                    <div className="txt">
                                        NO.2<br/>消息推送
                                    </div>
                                </li>
                                <li className={this.state.animation[2]===1?"no3Box no3A":"no3Box"}>
                                    <div className="txt">
                                        NO.3<br/>云端群呼
                                    </div>
                                </li>
                                <li className="no4Box">
                                    <div className="txt">
                                        NO.4<br/>语音通知
                                    </div>
                                </li>
                                <li className="no5Box">
                                    <div className="txt">
                                        NO.5<br/>语音会议
                                    </div>
                                </li>
                                <li className="no6Box">
                                    <div className="txt">
                                        NO.6<br/>流量统付
                                    </div>
                                </li>
                                <li className="no7Box">
                                    <div className="txt">
                                        NO.7<br/>保密小号
                                    </div>
                                </li>
                                <li className="no8Box">
                                    <div className="txt">
                                        NO.8<br/>模板短信
                                    </div>
                                </li>
                                <li className="no9Box">
                                    <div className="txt">
                                        NO.9<br/><span>营业厅取号</span>
                                    </div>
                                </li>
                            </ul>
                        </Col>
                    </Row>
                </div>
                <div className="dataArea">
                    <Row>
                        <Col xs={6}>
                            <div className="dataLine fl">
                                <img className="fl" src="img/taobao.png"/>
                                <span className="fl block">手机淘宝累计用户</span>
                                <div className="numLine small fl">
                                    {this.state.userOfTaobaoFormat.map((item,i) => (
                                        item === ","?(
                                            <div className="point" key={i}>{item}</div>
                                        ):(
                                            <div className={"numBox Index"+item}>
                                                <ul className="numList">
                                                    <li key={0}>0</li>
                                                    <li key={1}>1</li>
                                                    <li key={2}>2</li>
                                                    <li key={3}>3</li>
                                                    <li key={4}>4</li>
                                                    <li key={5}>5</li>
                                                    <li key={6}>6</li>
                                                    <li key={7}>7</li>
                                                    <li key={8}>8</li>
                                                    <li key={9}>9</li>
                                                </ul>
                                            </div>
                                        )
                                    ))}
                                </div>
                            </div>
                            <div className="dataLine fl">
                                <img className="fl" src="img/chinamobile.png"/>
                                <span className="fl block">浙江手厅累计用户</span>
                                <div className="numLine small fl">
                                    {this.state.userOfChinamobileFormat.map((item,i) => (
                                        item === ","?(
                                            <div className="point" key={i}>{item}</div>
                                        ):(
                                            <div className={"numBox Index"+item}>
                                                <ul className="numList">
                                                    <li key={0}>0</li>
                                                    <li key={1}>1</li>
                                                    <li key={2}>2</li>
                                                    <li key={3}>3</li>
                                                    <li key={4}>4</li>
                                                    <li key={5}>5</li>
                                                    <li key={6}>6</li>
                                                    <li key={7}>7</li>
                                                    <li key={8}>8</li>
                                                    <li key={9}>9</li>
                                                </ul>
                                            </div>
                                        )
                                    ))}
                                </div>
                            </div>
                            <div className="dataLine fl">
                                <img className="fl" src="img/news.png"/>
                                <span className="fl block"><i>壹点灵</i>累计用户</span>
                                <div className="numLine small fl">
                                    {this.state.userOfNewsFormat.map((item,i) => (
                                        item === ","?(
                                            <div className="point" key={i}>{item}</div>
                                        ):(
                                            <div className={"numBox Index"+item}>
                                                <ul className="numList">
                                                    <li key={0}>0</li>
                                                    <li key={1}>1</li>
                                                    <li key={2}>2</li>
                                                    <li key={3}>3</li>
                                                    <li key={4}>4</li>
                                                    <li key={5}>5</li>
                                                    <li key={6}>6</li>
                                                    <li key={7}>7</li>
                                                    <li key={8}>8</li>
                                                    <li key={9}>9</li>
                                                </ul>
                                            </div>
                                        )
                                    ))}
                                </div>
                            </div>
                        </Col>
                        <Col xs={6}>
                            <div className="dataLine type2 fl">
                                <span className="fl block1">拨打验证累计调用</span>
                                <div className="numLine small fl">
                                    {this.state.manyVoiceFormat.map((item,i) => (
                                        item === ","?(
                                            <div className="point" key={i}>{item}</div>
                                        ):(
                                            <div className={"numBox Index"+item}>
                                                <ul className="numList">
                                                    <li key={0}>0</li>
                                                    <li key={1}>1</li>
                                                    <li key={2}>2</li>
                                                    <li key={3}>3</li>
                                                    <li key={4}>4</li>
                                                    <li key={5}>5</li>
                                                    <li key={6}>6</li>
                                                    <li key={7}>7</li>
                                                    <li key={8}>8</li>
                                                    <li key={9}>9</li>
                                                </ul>
                                            </div>
                                        )
                                    ))}
                                </div>
                                <span className="fl other">次</span>
                            </div>
                            <div className="dataLine type2 fl">
                                <span className="fl block1">消息推送累计调用</span>
                                <div className="numLine small fl">
                                    {this.state.messageFormat.map((item,i) => (
                                        item === ","?(
                                            <div className="point" key={i}>{item}</div>
                                        ):(
                                            <div className={"numBox Index"+item}>
                                                <ul className="numList">
                                                    <li key={0}>0</li>
                                                    <li key={1}>1</li>
                                                    <li key={2}>2</li>
                                                    <li key={3}>3</li>
                                                    <li key={4}>4</li>
                                                    <li key={5}>5</li>
                                                    <li key={6}>6</li>
                                                    <li key={7}>7</li>
                                                    <li key={8}>8</li>
                                                    <li key={9}>9</li>
                                                </ul>
                                            </div>
                                        )
                                    ))}
                                </div>
                                <span className="fl other">次</span>
                            </div>
                            <div className="dataLine type2 fl">
                                <span className="fl block1">云端群呼累计调用</span>
                                <div className="numLine small fl">
                                    {this.state.smsTemplateFormat.map((item,i) => (
                                        item === ","?(
                                            <div className="point" key={i}>{item}</div>
                                        ):(
                                            <div className={"numBox Index"+item}>
                                                <ul className="numList">
                                                    <li key={0}>0</li>
                                                    <li key={1}>1</li>
                                                    <li key={2}>2</li>
                                                    <li key={3}>3</li>
                                                    <li key={4}>4</li>
                                                    <li key={5}>5</li>
                                                    <li key={6}>6</li>
                                                    <li key={7}>7</li>
                                                    <li key={8}>8</li>
                                                    <li key={9}>9</li>
                                                </ul>
                                            </div>
                                        )
                                    ))}
                                </div>
                                <span className="fl other">次</span>
                            </div>
                        </Col>
                    </Row>
                </div>
                {this.props.children}
            </div>
        )
    }
}
module.exports = zjComponent;
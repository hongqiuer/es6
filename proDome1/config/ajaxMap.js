import cookie from './cookies'
module.exports = {
    ajaxPath:cookie&&cookie.rootPath?cookie.rootPath:"",
    list:{
        url:"/getComInfoList.action",
        type:"post",
        parms:{}
    },
    detail:{
        url:"/detail.action",
        type:"post",
        parms:{}
    },
    errorStatus:{
        "3001":"系统异常",
        "3002":"数据格式错误"
    },
    checkError:function(_r,_hashHistory){
        if(_r&&this.errorStatus[_r]){
            _hashHistory.push("/errorPage/"+_r+"?msg="+this.errorStatus[_r])
        }
    },
    getAjaxUrl:function(_action){
        var _a = this[_action];
        if(_a){
            return this["ajaxPath"]+_a.url;
        }
        return "";
    },
    getAjaxParms:function(_action){
        var _a = this[_action];
        if(_a){
            return {url:this["ajaxPath"]+_a.url,type:_a.type,data:_a.parms,dataType:'json'};
        }
        return {};
    },
    setParm:function(_action,_parm,_v){
        if(typeof _v !== "undefined"){
            this[_action].parms[_parm] = _v;
        }
    },
    setParms:function(_action,_parms){
        if(typeof _parms === "object"){
            this[_action].parms = _parms;
        }
    }
}

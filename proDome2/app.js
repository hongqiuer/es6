/**
 * Created by hqer on 2017/3/28.
 */
import './less/main.less'
import React from 'react'
import { render } from 'react-dom'
import {HashRouter as Router,Route,Redirect} from 'react-router-dom'

import Global from './config/global'
import Header from "./module/header"
import Left from "./module/leftMenu"
import OutLine from "./module/outLine"
import Input from "./module/Input"
import DropDown from "./module/DropDown"
import CheckBox from "./module/CheckBox"
import Upload from "./module/Upload"
import Pagination from "./module/Pagination"

render((
    <Router>
        <div className="UILib">
            <Left/>
            <Header/>
            <div className="contentArea">
                <Route exact path="/" render={() => (<Redirect to="/OutLine"/>)}/>
                <Route path="/OutLine" component={OutLine}/>
                <Route path="/Input" component={Input}/>
                <Route path="/DropDown" component={DropDown}/>
                <Route path="/CheckBox" component={CheckBox}/>
                <Route path="/Upload" component={Upload}/>
                <Route path="/Pagination" component={Pagination}/>
            </div>
        </div>
    </Router>
), document.getElementById('app'));
